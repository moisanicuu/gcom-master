package se.umu.cs.jsgajn.gcom.dao.connection.repositories;



import lombok.AccessLevel;
import lombok.Getter;
import se.umu.cs.jsgajn.gcom.dao.connection.SQLDatabaseConnection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("unchecked")
@Getter
public class CrudRepositoryImpl<T> implements CrudRepository<T> {
    @Getter(AccessLevel.NONE)
    private final Class<T> type;
    protected Connection conn;

    CrudRepositoryImpl(SQLDatabaseConnection SQLDatabaseConnection) {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.conn = SQLDatabaseConnection.connection();
    }

    public List<T> selectAll() throws Exception {
        ArrayList<T> info = new ArrayList<>();
        T obj = type.newInstance();
        PreparedStatement preparedStatement = conn
                .prepareStatement("Select * from " + obj.getClass().getSimpleName(),
                                        ResultSet.TYPE_SCROLL_SENSITIVE,
                                        ResultSet.CONCUR_UPDATABLE);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.first();
        do {
            int i = 1;
            T newObj = (T) obj.getClass().newInstance();

            buildObject(obj, resultSet, i, newObj, obj.getClass().getDeclaredMethods());

            info.add(newObj);
        } while (resultSet.next());
        preparedStatement.close();
        return info;
    }

    private void buildObject(T obj, ResultSet resultSet, int i, T newObj, Method[] declaredMethods)
            throws IllegalAccessException, InvocationTargetException, SQLException {
        for (Field field : obj.getClass().getDeclaredFields())
            for (Method method : declaredMethods)
                if (method.getName().toLowerCase().contains("set" + field.getName().toLowerCase())) {
                    if (field.getType().toString().compareTo("int") == 0)
                        method.invoke(newObj, resultSet.getInt(i++));
                    else
                        method.invoke(newObj, resultSet.getString(i++));

                }
    }

    public void insert(T obj) throws Exception {
        String insertQuery;
        StringBuilder values = new StringBuilder();
        StringBuilder insertQueryBuilder = new StringBuilder("insert into " + obj.getClass().getSimpleName() + "(");
        for (Field field : obj.getClass().getDeclaredFields())
            if ( !field.getName().toLowerCase().contains("id")) {
                insertQueryBuilder.append(field.getName()).append(", ");
                for (Method method : obj.getClass().getDeclaredMethods())
                    if (method.getName().toLowerCase().contains("get" + field.getName().toLowerCase()))
                        if (method.invoke(obj) == null)
                            insertQueryBuilder = new StringBuilder(insertQueryBuilder.substring(0, insertQueryBuilder.length() - field.getName().length() - 2));
                        else
                            values.append("\'").append(method.invoke(obj)).append("\', ");
            }
        insertQuery = insertQueryBuilder.toString();

        insertQuery = insertQuery.substring(0, insertQuery.length() - 2);
        values = new StringBuilder(values.substring(0, values.length() - 2));
        insertQuery += ") values (";
        insertQuery += values + ");";
        PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);
        preparedStatement.executeUpdate();
        preparedStatement.close();
        Optional<T> newEntryOptional = getLatestEntry();
        if(newEntryOptional.isPresent()){
            T newEntry = newEntryOptional.get();
            for (Method setMethod : obj.getClass().getDeclaredMethods())
                if (setMethod.getName().toLowerCase().contains("set") && setMethod.getName().toLowerCase().contains("id"))
                {
                    for (Method getMethod : obj.getClass().getDeclaredMethods())
                        if (getMethod.getName().toLowerCase().contains("get") && getMethod.getName().toLowerCase().contains("id"))
                            setMethod.invoke(obj, getMethod.invoke(newEntry));
                }
        }

    }

    public void update(T obj) throws Exception {
        String updateQuery;
        String primaryKey = null;
        int primaryKeysValue = -1;
        StringBuilder updateQueryBuilder = new StringBuilder("update " + obj.getClass().getSimpleName() + " set ");
        for (Field field : obj.getClass().getDeclaredFields()) {
            for (Method method : obj.getClass().getDeclaredMethods())
                if (method.getName().toLowerCase().contains("get" + field.getName().toLowerCase()))
                    if (field.getName().toLowerCase().contains("id")) {
                        primaryKey = field.getName();

                        primaryKeysValue = (int) method.invoke(obj);

                    } else {
                        updateQueryBuilder.append(field.getName()).append(" = ");
                        updateQueryBuilder.append("\'").append(method.invoke(obj)).append("\', ");
                    }
        }
        updateQuery = updateQueryBuilder.toString();

        updateQuery = updateQuery.substring(0, updateQuery.length() - 2);
        updateQuery += " where " + primaryKey + " = " + primaryKeysValue + ";";

        PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public void delete(int id) throws Exception {
        String deleteQuery = String.format("delete from %s where id = %d;", type.getSimpleName(), id);

        PreparedStatement preparedStatement = conn.prepareStatement(deleteQuery);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public Optional<T> selectById(int id) throws Exception {
        String selectByIdQuery = String.format("Select * from %s where id=%d", type.getSimpleName(), id);
        return executeSelectSingleEntryQuery(selectByIdQuery);
    }

    @Override
    public Optional<T> getLatestEntry() throws Exception {
        String selectByIdQuery = String.format("select * from %s where id=(select max(id) from %s);", type.getSimpleName(), type.getSimpleName());
        return executeSelectSingleEntryQuery(selectByIdQuery);
    }

     Optional<T> executeSelectSingleEntryQuery(String selectByIdQuery) throws InstantiationException, IllegalAccessException, SQLException, InvocationTargetException {
        T obj = type.newInstance();
        PreparedStatement preparedStatement = conn.prepareStatement(selectByIdQuery,
                ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        ResultSet result = preparedStatement.executeQuery();

        if (!result.first())
            return Optional.empty();
        T newObj = (T) obj.getClass().newInstance();
        int i = 1;
        buildObject(obj, result, i, newObj, newObj.getClass().getDeclaredMethods());
        preparedStatement.close();
        return Optional.of(newObj);
    }



}
