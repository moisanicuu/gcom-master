package se.umu.cs.jsgajn.gcom.dao.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ChatMessage {
    private int id;
    private String message;
    private LocalDateTime time;
    private ChatGroup chatGroup;
    private ChatClient chatClient;
}
