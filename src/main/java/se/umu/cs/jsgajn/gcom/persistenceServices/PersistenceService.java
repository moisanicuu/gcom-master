package se.umu.cs.jsgajn.gcom.persistenceServices;

import se.umu.cs.jsgajn.gcom.dao.connection.SQLDatabaseConnection;
import se.umu.cs.jsgajn.gcom.dao.connection.repositories.ChatGroupRepository;
import se.umu.cs.jsgajn.gcom.dao.connection.repositories.ChatClientRepository;
import se.umu.cs.jsgajn.gcom.dao.connection.repositories.ChatMessageRepository;
import se.umu.cs.jsgajn.gcom.dao.model.ChatClient;
import se.umu.cs.jsgajn.gcom.dao.model.ChatGroup;
import se.umu.cs.jsgajn.gcom.dao.model.ChatMessage;

import javax.swing.*;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersistenceService {
    private SQLDatabaseConnection sqlDatabaseConnection;
    private ChatClientRepository chatClientRepository;
    private ChatGroupRepository chatGroupRepository;
    private ChatMessageRepository chatMessageRepository;
    public PersistenceService(String host) {
        try {
            this.sqlDatabaseConnection = new SQLDatabaseConnection(host,"postgres", "pass");
            chatClientRepository = new ChatClientRepository(sqlDatabaseConnection);
            chatGroupRepository = new ChatGroupRepository(sqlDatabaseConnection);
            chatMessageRepository = new ChatMessageRepository(sqlDatabaseConnection);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public Optional<ChatGroup> getChatGroupByName(String groupName) {
        return chatGroupRepository.getChatGroupByName(groupName);
    }

    public void saveChatGroup(ChatGroup chatGroup) {
        try {
            chatGroupRepository.insert(chatGroup);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void saveChatMessage(String messengerName, String message, String chatGroup) {
        Optional<ChatClient> chatClientOptional = chatClientRepository.getChatClientByName(messengerName);
        Optional<ChatGroup> chatGroupOptional = chatGroupRepository.getChatGroupByName(chatGroup);
        if(chatGroupOptional.isPresent())
            if(chatClientOptional.isPresent()){
                ChatMessage chatMessage = new ChatMessage(0, message, LocalDateTime.now(), chatGroupOptional.get(), chatClientOptional.get());
                try {
                    chatMessageRepository.insert(chatMessage);
                } catch (Exception e) {
                    e.printStackTrace();
//                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
                }
            }
            else {
                ChatClient chatClient = new ChatClient(0, messengerName);
                try {
                    chatClientRepository.insert(chatClient);
                    ChatMessage chatMessage = new ChatMessage(0, message, LocalDateTime.now(), chatGroupOptional.get(), chatClient);
                    chatMessageRepository.insert(chatMessage);
                } catch (Exception e) {
                    e.printStackTrace();
//                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
                }
            }
        else
            System.out.println("mouie");
//            JOptionPane.showMessageDialog(null, "Something went wrong fetching the group!", "Error!", JOptionPane.WARNING_MESSAGE);

    }

    public void addClientToChatGroup(String clientName, String groupName) {
        Optional<ChatClient> chatClientOptional = chatClientRepository.getChatClientByName(clientName);
        Optional<ChatGroup> chatGroupOptional = chatGroupRepository.getChatGroupByName(groupName);
        if(chatGroupOptional.isPresent())
            if(chatClientOptional.isPresent()){
                try {
                    if(!chatClientRepository.checkClientInGroup(chatGroupOptional.get().getId(), chatClientOptional.get().getId()))
                        chatGroupRepository.addClientToChatGroup(chatGroupOptional.get().getId(), chatClientOptional.get().getId());
                } catch (Exception e) {
                    e.printStackTrace();
//                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
                }
            }
            else {
                try {
                    ChatClient chatClient = new ChatClient(0, clientName);
                    chatClientRepository.insert(chatClient);
                    chatGroupRepository.addClientToChatGroup(chatGroupOptional.get().getId(), chatClient.getId());

                } catch (Exception e) {
                    e.printStackTrace();
//                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
                }
            }
        else
            System.out.println("mouie");
    }

    public void removeClientFromChatGroup(String clientName, String groupName) {
        Optional<ChatClient> chatClientOptional = chatClientRepository.getChatClientByName(clientName);
        Optional<ChatGroup> chatGroupOptional = chatGroupRepository.getChatGroupByName(groupName);
        if (chatGroupOptional.isPresent() && chatClientOptional.isPresent()) {
            try {
                chatGroupRepository.removeClientFromChatGroup(chatGroupOptional.get().getId(), chatClientOptional.get().getId());
            } catch (Exception e) {
                e.printStackTrace();
//                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public List<ChatClient> getClientsForChatGroup(String groupName) {
        return chatGroupRepository.getClientsForChatGroup(groupName);
    }

    public List<ChatMessage> getChatMessagesForGroup(String groupName) {
        Optional<ChatGroup> chatGroupOptional = chatGroupRepository.getChatGroupByName(groupName);
        if (chatGroupOptional.isPresent()){
            try {
                return chatMessageRepository.getChatMessagesForGroup(chatGroupOptional.get().getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    public void disconnect() {
        try {
            this.sqlDatabaseConnection.disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
