package se.umu.cs.jsgajn.gcom.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;

public class SQLDatabaseConnection {
    private Connection conn;
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String TEMPLATE_URL = "jdbc:postgresql://host:5432/chat_database";
    private final String url;

    public SQLDatabaseConnection(String host, String username, String password) throws Exception {
        Class.forName(DRIVER);
        url = TEMPLATE_URL.replace("host", host);
        conn = DriverManager.getConnection(url, username, password);
        System.out.println("Connected");
    }

    public void disconnect() throws SQLException {
        conn.close();
        conn = null;
    }

    public Connection connection() {
        return conn;
    }
}