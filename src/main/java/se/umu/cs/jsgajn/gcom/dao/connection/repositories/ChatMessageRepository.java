package se.umu.cs.jsgajn.gcom.dao.connection.repositories;

import se.umu.cs.jsgajn.gcom.dao.connection.SQLDatabaseConnection;
import se.umu.cs.jsgajn.gcom.dao.model.ChatClient;
import se.umu.cs.jsgajn.gcom.dao.model.ChatGroup;
import se.umu.cs.jsgajn.gcom.dao.model.ChatMessage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChatMessageRepository extends CrudRepositoryImpl<ChatMessage> {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private final ChatGroupRepository CHAT_GROUP_REPOSITORY;
    private final ChatClientRepository CLIENT_REPOSITORY;

    public ChatMessageRepository(SQLDatabaseConnection SQLDatabaseConnection) {
        super(SQLDatabaseConnection);
        CHAT_GROUP_REPOSITORY = new ChatGroupRepository(SQLDatabaseConnection);
        CLIENT_REPOSITORY = new ChatClientRepository(SQLDatabaseConnection);
    }

    @Override
    public List<ChatMessage> selectAll() throws Exception {
        PreparedStatement preparedStatement = getConn()
                .prepareStatement("Select * from chatMessage",
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ChatMessage> chatMessages = fetchChatMessages(resultSet);
        preparedStatement.close();
        return chatMessages;
    }

    @Override
    public void insert(ChatMessage chatMessage) throws Exception {
        PreparedStatement preparedStatement = getConn()
                .prepareStatement("insert into chatMessage(message, time, groupid, clientid) values (?, ?, ?, ?)");
        preparedStatement.setString(1, chatMessage.getMessage());
        preparedStatement.setString(2, chatMessage.getTime().format(DATE_TIME_FORMATTER));
        preparedStatement.setInt(3, chatMessage.getChatGroup().getId());
        preparedStatement.setInt(4, chatMessage.getChatClient().getId());

        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void update(ChatMessage chatMessage) throws Exception {
        PreparedStatement preparedStatement = getConn()
                .prepareStatement("update chatMessage set message = ?, time = ?, groupid = ?, clientid = ? where id = ?");
        preparedStatement.setString(1, chatMessage.getMessage());
        preparedStatement.setString(2, chatMessage.getTime().format(DATE_TIME_FORMATTER));
        preparedStatement.setInt(3, chatMessage.getChatGroup().getId());
        preparedStatement.setInt(4, chatMessage.getChatClient().getId());
        preparedStatement.setInt(5, chatMessage.getId());

        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public Optional<ChatMessage> selectById(int id) throws Exception {
        PreparedStatement preparedStatement = getConn()
                .prepareStatement("Select * from chatMessage",
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);

        ResultSet resultSet = preparedStatement.executeQuery();
        List<ChatMessage> chatMessages = fetchChatMessages(resultSet);
        preparedStatement.close();
        if (chatMessages.size() == 1)
            return Optional.of(chatMessages.get(0));
        if (chatMessages.size() == 0)
            return Optional.empty();
        else
            throw new Exception("Multiple entities with the same id found!");
    }

    public List<ChatMessage> getChatMessagesForGroup(int groupId) throws Exception {
        PreparedStatement preparedStatement = conn
                .prepareStatement("Select * from chatMessage where groupid = " + groupId,
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ChatMessage> chatMessages = fetchChatMessages(resultSet);
        preparedStatement.close();
        return chatMessages;
    }

    private List<ChatMessage> fetchChatMessages(ResultSet resultSet) throws Exception {
        List<ChatMessage> chatMessages = new ArrayList<>();

        while (resultSet.next()) {
            int clientId = resultSet.getInt("clientId");
            int groupId = resultSet.getInt("groupId");
            Optional<ChatClient> clientOptional = CLIENT_REPOSITORY.selectById(clientId);
            Optional<ChatGroup> chatGroupOptional = CHAT_GROUP_REPOSITORY.selectById(groupId);
            ChatClient chatClient;
            ChatGroup chatGroup;
            if (clientOptional.isPresent())
                chatClient = clientOptional.get();
            else
                throw new Exception("Could not fetch client entity with id = " + clientId);
            if (chatGroupOptional.isPresent())
                chatGroup = chatGroupOptional.get();
            else
                throw new Exception("Could not fetch chatGroup entity with id = " + clientId);

            ChatMessage chatMessage = ChatMessage
                    .builder()
                    .id(resultSet.getInt("id"))
                    .message(resultSet.getString("message"))
                    .time(LocalDateTime
                            .parse(resultSet.getString("time"),
                                    DATE_TIME_FORMATTER))
                    .chatGroup(chatGroup)
                    .chatClient(chatClient)
                    .build();
            chatMessages.add(chatMessage);
        }
        return chatMessages;
    }
}
