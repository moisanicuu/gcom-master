package se.umu.cs.jsgajn.gcom.testapp;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class LogoutMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
}
