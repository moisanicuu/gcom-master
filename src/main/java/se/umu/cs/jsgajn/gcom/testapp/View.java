package se.umu.cs.jsgajn.gcom.testapp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.EventHandler;

public class View extends JFrame {

    private Controller controller;
    private Model model;

    public View(Controller controller, Model model) {

        this.controller = controller;
        this.model = model;

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(controller != null)
                    controller.exit();
                System.exit(0);
            }
        });

        this.model.setFrame(this);
        this.setTitle("Chat");

        JPanel panel = (JPanel) this.getContentPane();
        panel.setBackground(new Color(242, 242, 242));
        GridBagLayout gridBagLayout0 = new GridBagLayout();
        gridBagLayout0.columnWidths = new int[]{12, 20, 6, 0, 6, 150, 12};
        gridBagLayout0.rowHeights = new int[]{6, 25, 6, 20, 6, 25, 12};
        gridBagLayout0.columnWeights = new double[]{0, 1, 0, 0, 0, 0, 0};
        gridBagLayout0.rowWeights = new double[]{0, 0, 0, 1, 0, 0, 0};
        panel.setLayout(gridBagLayout0);

        // Back button
        JButton back = new JButton();
        back.setText("Log out");
        back.setBackground(new Color(59, 89, 182));
        back.setForeground(Color.WHITE);
        back.setFocusPainted(false);
        back.setFont(new Font("Roboto", Font.BOLD, 12));
        JFrame frame = this;
        back.addActionListener(e -> {
            frame.setVisible(false);
            controller.exit();
            this.openConfigureConnectionView();
        });
        panel.add(back, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.LINE_START, 3, new Insets(0, 0, 0, 0), 0, 0));

        // Exit button
        JButton exit = new JButton();
        exit.setText("Exit");
        exit.setBackground(new Color(255, 77, 77));
        exit.setForeground(Color.WHITE);
        exit.setFocusPainted(false);
        exit.setFont(new Font("Roboto", Font.BOLD, 12));
        exit.addActionListener(EventHandler.create(ActionListener.class, controller, "exit"));
        panel.add(exit, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, 14, 3, new Insets(0, 0, 0, 0), 0, 0));

        // Send button
        JButton send = new JButton();
        send.setText("Send");
        send.setBackground(new Color(59, 89, 182));
        send.setForeground(Color.WHITE);
        send.setFocusPainted(false);
        send.setFont(new Font("Roboto", Font.BOLD, 12));
        send.addActionListener(EventHandler.create(ActionListener.class, controller, "send"));
        panel.add(send, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, 14, 3, new Insets(0, 0, 0, 0), 0, 0));

        // Member list
        JList members = new JList();
        DefaultListModel membersModel = new DefaultListModel();
        members.setModel(membersModel);
        JScrollPane membersScrollPane = new JScrollPane(members);
        membersScrollPane.setPreferredSize(new Dimension(23, 23));
        panel.add(membersScrollPane, new GridBagConstraints(5, 3, 1, 3, 0.0, 0.0, 15, 1, new Insets(0, 0, 0, 0), 0, 0));
        this.model.setMembers(membersModel);

        // Chat messages
        JTextArea chatMessages = new JTextArea();
        chatMessages.setEditable(false);
        JScrollPane chatMessagesScrollPane = new JScrollPane(chatMessages);
        chatMessagesScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(chatMessagesScrollPane, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, 13, 1, new Insets(0, 0, 0, 0), 0, 0));
        this.model.setChat(chatMessages);

        // Write message-box
        JTextArea writeMessage = new JTextArea();
        writeMessage.setFont(writeMessage.getFont().deriveFont(15f));
        writeMessage.setText("Type something...");
        writeMessage.setLineWrap(true);
        writeMessage.setWrapStyleWord(true);
        JScrollPane writeMessageScrollPane = new JScrollPane(writeMessage);
        writeMessage.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(writeMessage.getText().equals("Type something...")){
                    writeMessage.setText("");
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        panel.add(writeMessageScrollPane, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, 15, 1, new Insets(0, 0, 0, 0), 0, 0));
        this.model.setMessage(writeMessage);

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setBounds(new Rectangle(500, 0, 835, 528));

        this.openConfigureConnectionView();
        controller.init();
    }

    public void openConfigureConnectionView() {
        JFormattedTextField host = new JFormattedTextField();
        JFormattedTextField nick = new JFormattedTextField();
        JFormattedTextField channel = new JFormattedTextField();
        JFormattedTextField hostPort = new JFormattedTextField();
        JFormattedTextField localPort = new JFormattedTextField();

        host.setText("192.168.43.111");
        nick.setText("Nicu");
        channel.setText("Dist");
        hostPort.setText("1078");
        localPort.setText("33444");

        JButton login = new JButton();
        login.setText("Login");
        login.setBackground(new Color(59, 89, 182));
        login.setForeground(Color.WHITE);
        login.setFocusPainted(false);
        login.setFont(new Font("Roboto", Font.BOLD, 12));

        this.model.setHost(host);
        this.model.setNick(nick);
        this.model.setChannel(channel);
        this.model.setHostPort(hostPort);
        this.model.setLocalPort(localPort);

        ConnectDialog connectDialog = new ConnectDialog(host, nick, channel, hostPort, localPort);

        JButton[] buttons = { login };
        int res = JOptionPane.showOptionDialog(null, connectDialog, "Connect", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
        if(res == -1) { // daca am apasat pe x inchide sistemul
            System.exit(0);
        }
    }

}
