package se.umu.cs.jsgajn.gcom.dao.connection.repositories;

import se.umu.cs.jsgajn.gcom.dao.connection.SQLDatabaseConnection;
import se.umu.cs.jsgajn.gcom.dao.model.ChatClient;
import se.umu.cs.jsgajn.gcom.dao.model.ChatGroup;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChatGroupRepository extends CrudRepositoryImpl<ChatGroup> {
    public ChatGroupRepository(SQLDatabaseConnection SQLDatabaseConnection) {
        super(SQLDatabaseConnection);
    }

    public Optional<ChatGroup> getChatGroupByName(String name) {
        String query = "select * from chatGroup where groupName = '" + name +"';";
        try {
            return executeSelectSingleEntryQuery(query);
        } catch (InstantiationException | InvocationTargetException | SQLException | IllegalAccessException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error!", JOptionPane.WARNING_MESSAGE);
        }
        return Optional.empty();
    }

    public void addClientToChatGroup(int groupId, int clientId) {
        String query = "insert into chatGroupClient(groupId, clientId) values (" + groupId + ", " + clientId + ");";
        executeUpdateQuery(query);
    }

    public void removeClientFromChatGroup(int groupId, int clientId) {
        String query = "delete from chatGroupClient where groupId= '" + groupId + "' and clientId='" + clientId + "';";
        executeUpdateQuery(query);
    }

    private void executeUpdateQuery(String query) {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(query,
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<ChatClient> getClientsForChatGroup(String groupName) {
        List<ChatClient> chatClients = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn
                    .prepareStatement("select c.id, c.nickname from chatClient c join chatGroupClient on (c.id = clientid) " +
                                    "                                        join chatgroup c2 on (c2.id = groupid) where c2.groupname = '" + groupName +"';",
                            ResultSet.TYPE_SCROLL_SENSITIVE,
                            ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.first();
            do {
                ChatClient chatClient = new ChatClient(resultSet.getInt(1), resultSet.getString(2));
                chatClients.add(chatClient);
            } while (resultSet.next());

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        if(preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
        return chatClients;
    }
}
