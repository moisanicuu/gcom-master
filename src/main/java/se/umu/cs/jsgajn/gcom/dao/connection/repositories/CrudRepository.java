package se.umu.cs.jsgajn.gcom.dao.connection.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    List<T> selectAll() throws Exception;
    void insert(T obj) throws Exception;
    void update(T obj) throws Exception;
    void delete(int id) throws Exception;
    Optional<T> selectById(int id) throws Exception;
    Optional<T> getLatestEntry() throws Exception;
}
