package se.umu.cs.jsgajn.gcom.dao.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ChatGroup {
    private int id;
    private String groupName;
    private String multicastType;
    private String orderingType;
}
