package se.umu.cs.jsgajn.gcom.dao.connection.repositories;

import se.umu.cs.jsgajn.gcom.dao.connection.SQLDatabaseConnection;
import se.umu.cs.jsgajn.gcom.dao.model.ChatClient;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class ChatClientRepository extends CrudRepositoryImpl<ChatClient> {

    public ChatClientRepository(SQLDatabaseConnection SQLDatabaseConnection) {
        super(SQLDatabaseConnection);
    }

    public Optional<ChatClient> getChatClientByName(String name) {
        String query = "select * from chatClient where nickname = '" + name +"';";
        try {
            return executeSelectSingleEntryQuery(query);
        } catch (InstantiationException | InvocationTargetException | SQLException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public boolean checkClientInGroup(int groupId, int clientId){
        String query = "select * from chatGroupClient where groupId= " + groupId + " and clientId=" + clientId + ";";
        try {
            return executeSelectSingleEntryQuery(query).isPresent();
        } catch (InstantiationException | InvocationTargetException | SQLException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }
}
