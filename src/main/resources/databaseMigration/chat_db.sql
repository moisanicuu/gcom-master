create table if not exists testTable
(
    id   serial primary key,
    name varchar(20)
);

create table if not exists chatClient
(
    id       serial primary key,
    nickname varchar(32) uniquecha
);

create table if not exists chatGroup
(
    id            serial primary key,
    groupName     varchar(32) unique,
    multicastType varchar(20) check (multicastType in ('BASIC_MULTICAST', 'RELIABLE_MULTICAST')),
    orderingType  varchar(32) check (orderingType in
                                     ('FIFO', 'TOTAL_ORDER', 'NO_ORDERING', 'CASUAL_ORDERING', 'CASUAL_TOTAL_ORDERING'))
);
create table if not exists chatGroupClient (
   groupId  int,
   clientId int,
   primary key (groupId, clientId),
   foreign key (groupId) references chatGroup (id) on delete cascade on update cascade,
   foreign key (clientId) references chatClient (id) on delete cascade on update cascade
);

create table if not exists chatMessage
(
    id       serial primary key,
    message  text,
    time     varchar(32),
    groupId  int,
    clientId int,
    foreign key (groupId) references chatGroup (id) on delete cascade on update cascade,
    foreign key (clientId) references chatClient (id) on delete cascade on update cascade
);